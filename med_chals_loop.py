import sys
import time
import os

while True:
    print("starting bpsk 900 MHz tx")
    print("===========================")
    os.system("python utils/file_tx.py -w images_tx/cyber-image_500ksps_bpsk.fc32 -f 907500000 -r 500000 -g 50")
    print("done - configuring next tx")
    time.sleep(5)

    print("starting fsk 900 MHz tx")
    print("===========================")
    os.system("python utils/file_tx.py -w images_tx/cyber-image_500ksps_fsk.fc32 -f 905500000 -r 500000 -g 50")
    print("done - configuring next tx")
    time.sleep(5)
