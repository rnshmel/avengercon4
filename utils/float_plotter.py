import numpy as np
import sys
import matplotlib.pyplot as plt

file_name = str(sys.argv[1]) # name of IQ file
x = np.fromfile(file_name, dtype="float32")
x = x.astype("float16")
plt.plot(x)
plt.show()
