"""
Generate and TX samples using a set of waveforms, and waveform characteristics
"""

import argparse
import uhd
import sys
import time
import numpy as np

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--args", default="", type=str)
    parser.add_argument("-w", "--waveform", default="", type=str, required=True)
    parser.add_argument("-f", "--freq", type=float, required=True)
    parser.add_argument("-r", "--rate", default=1e6, type=float)
    parser.add_argument("-c", "--channels", default=0, nargs="+", type=int)
    parser.add_argument("-g", "--gain", type=int, default=50)
    return parser.parse_args()

def main():
    """TX samples based on input arguments"""
    args = parse_args()
    usrp = uhd.usrp.MultiUSRP(args.args)
    if not isinstance(args.channels, list):
        args.channels = [args.channels]
    data = np.fromfile(args.waveform, dtype="complex64")
    duration = float((len(data)/args.rate)+.1)

    usrp.send_waveform(data, duration, args.freq, args.rate, args.channels, args.gain)
    sys.exit(0)

if __name__ == "__main__":
    main()
