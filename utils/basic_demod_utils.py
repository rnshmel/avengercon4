# basic tools for demodding IQ data from an SDR
# mod tools are:
# BPSK demod - in progress
# FSK demod - done

import numpy as np
import scipy as sci
import sys
import struct
import time
import scipy.signal as sig

# basic fsk demodulation
# takes an IQ array: must be a numpy array of complex 64 bit floats
# returns a numpy array of byte data, signed 8 bit ints (-1, 1)
# uses polar discriminator to get inst. phase between samples
# values > 0 are 1 and values <= 0 are -1
def fsk_demod_basic(iqInput):
    # polar discriminator
    polarDis = iqInput[1:] * np.conj(iqInput[:-1])
    polarDis = np.angle(polarDis)
    size = len(iqInput)
    # fill output byte array
    byteOut = np.zeros(size, dtype="int8")
    for i in range(0,size):
        if polarDis[i] > 0:
            byteOut[i] = 1
        else:
            byteOut[i] = -1
    byteOut[size-1] = byteOut[size-2]
    return byteOut

# basic clock recovery function
# takes a numpy array of demodded data (int8 dtype), a sample rate
# and a baud baud rate
# returns the clock recovered 1's and 0's array (numpy int8)
def clock_rec_basic(byteInputArray, sampRate, baud):
    size = len(byteInputArray)
    outArray = np.zeros(size, dtype="int8")
    sps = sampRate/baud
    # iterate along the array, until we find a flip
    # keep track of the number of counts
    preVal = byteInputArray[0]
    currPos = 0
    count = 1
    for i in range(1, size):
        if byteInputArray[i] != preVal:
            # add count/sps number of preVal to the arrays
            Amt = count/sps
            addAmt = round(count/sps) # calc add ammount
            if addAmt != 0:
                # if not zero
                for j in range(0, addAmt):
                    # loop through addAmt times
                    # set the out array at the current postition to preVal
                    outArray[currPos] = preVal
                    currPos = currPos + 1
            # reset flip state
            preVal = byteInputArray[i]
            count = 0
        else:
            # no flip
            count = count + 1
            preVal = byteInputArray[i]
    return outArray

# takes in array returned from basic clock recovery (int8)
# strips all trailing bits, converts from [-1,1] to [0,1]
# returned array is uint8
def clock_rec_basic_cleanup(inArray):
    size = len(inArray)
    count = 0
    index = 0
    while count < size:
        if inArray[count] == 0:
            index = count
            break
        else:
            count = count + 1
    outArray = inArray[:index]
    for i in range(0, len(outArray)):
        if outArray[i] == -1:
            outArray[i] = 0
    return outArray.astype("uint8")

def bit_slice_floats(iqInput):
    size = len(iqInput)
    # fill output byte array
    byteOut = np.zeros(size, dtype="int8")
    for i in range(0,size):
        if iqInput[i] > 0:
            byteOut[i] = 1
        else:
            byteOut[i] = -1
    return byteOut
