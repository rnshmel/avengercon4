# basic mod tools for generating IQ data to be played from an SDR
import numpy as np
import scipy as sci
import sys
import struct
import time
import scipy.signal as sig

# bpsk modulation
# takes a bytearray as input, along with sample rate and baud
# returns a numpy array (complex 64 type) of IQ data
def bpsk_mod(byteInput, baudRate, sampRate):
    sps = int(sampRate/baudRate) # get samples per symbol
    array_size = 8 * sps * len(byteInput) # size of numpy array
    outArray = np.zeros(array_size, dtype="complex64") # our np array
    # loop through all bytes in byteInput
    index = 0 # store next open position in the arrray
    for i in byteInput:
        for j in range(0,8):
            # for each byte, shift left and AND with 128
            # this will pop off the left bits in order
            # save as "val"
            val = (i << j) & 128
            if val == 128: # a "one"
                # add 1+0j sps-times to the np array
                for k in range(0,sps):
                    outArray[index] = 1+0j
                    index = index + 1
            else: # a "zero"
                # add sps numer of -1+0j to array
                for k in range(0,sps):
                    outArray[index] = -1+0j
                    index = index + 1
    # return the output array (numpy)
    return outArray

# qpsk modulation
# takes a bytearray as input, along with sample rate and baud
# returns a numpy array (complex 64 type) of IQ data
def qpsk_mod(byteInput, baudRate, sampRate):
    sps = int(sampRate/baudRate) # get samples per symbol
    array_size = 4 * sps * len(byteInput) # size of numpy array
    outArray = np.zeros(array_size, dtype="complex64") # our np array
    # loop through all bytes in byteInput
    index = 0 # store next open position in the arrray
    for i in byteInput:
        for j in range(0,8,2):
            # for each byte, shift left two and AND with 192
            # this will pop off the left two bits in order
            # save as "val"
            val = (i << j) & 192
            # "11" = 192
            # "10" = 128
            # "01" = 64
            # "00" = 00
            if val == 192: # a "11"
                # add 1+1j sps-times to the np array
                for k in range(0,sps):
                    outArray[index] = 1+1j
                    index = index + 1
            elif val == 128: # a "10"
                # add sps numer of 1-1j to array
                for k in range(0,sps):
                    outArray[index] = 1-1j
                    index = index + 1
            elif val == 64: # a "01"
                # add sps numer of -1+1j to array
                for k in range(0,sps):
                    outArray[index] = -1+1j
                    index = index + 1
            else: # a "00"
                # add sps numer of -1-1j to array
                for k in range(0,sps):
                    outArray[index] = -1-1j
                    index = index + 1
    # return the output array (numpy)
    return outArray

# fsk modulation
# takes a bytearray, sample rate, baud rate, and freq div
# freq div is in Hz
# returns a numpy array (complex 64 type) of IQ data
# NOTE: depreciated, use f_fsk_mod instead
def fsk_mod(byteInput, baudRate, sampRate, div):
    # print("fsk_mod is depreciated, use f_fsk_mod instead")
    f_high = int(div/2)
    f_low = int(-div/2)
    sps = int(sampRate/baudRate) # get samples per symbol
    array_size = 8 * sps * len(byteInput) # size of numpy array
    # make an array to store sample times for FSK signal
    timeArray = np.zeros(array_size, dtype="float32")
    # make sinArray and cosArray
    sinArray = np.zeros(array_size, dtype="float32")
    cosArray = np.zeros(array_size, dtype="float32")
    # make valArray
    valArray = np.zeros(array_size, dtype="uint8")

    # fill time array
    sampTime = 0.0
    for i in range(0,len(timeArray)):
        timeArray[i] = sampTime
        sampTime = sampTime + 1.0/sampRate

    # loop through all bytes in byteInput
    # fill valArray
    index = 0 # keep track of current index
    for i in byteInput:
        for j in range(0,8):
            # for each byte, shift left and & with 128
            # this will pop off the left bits in order
            # save as "val"
            val = (i << j) & 128
            if val == 128: # a one
                for k in range(0,sps):
                    valArray[index] = 1
                    index = index + 1
            else: # a zero
                for k in range(0,sps):
                    valArray[index] = 0
                    index = index + 1
    # fill sin and cos arrays
    for i in range(0,len(valArray)):
        if valArray[i] == 1:
            sinArray[i] = np.sin(2*np.pi*f_high*timeArray[i])
            cosArray[i] = np.cos(2*np.pi*f_high*timeArray[i])
        else:
            sinArray[i] = np.sin(2*np.pi*f_low*timeArray[i])
            cosArray[i] = np.cos(2*np.pi*f_low*timeArray[i])
    outArray = cosArray + 1j*sinArray
    outArray = outArray.astype("complex64")
    return outArray

# faster FSK modulation
def f_fsk_mod(byteInput, baudRate, sampRate, div):
    Ns = int(sampRate/baudRate)
    f0 = div
    fd = 1.5*div
    Nbits = len(byteInput) * 8
    N = Nbits * Ns
    valArray = np.zeros(N, dtype="int8")
    # fill valArray
    index = 0 # keep track of current index
    for i in byteInput:
        for j in range(0,8):
            # for each byte, shift left and & with 128
            # this will pop off the left bits in order
            # save as "val"
            val = (i << j) & 128
            if val == 128: # a one
                for k in range(0,Ns):
                    valArray[index] = 1
                    index = index + 1
            else: # a zero
                for k in range(0,Ns):
                    valArray[index] = 0
                    index = index + 1
    # compute phase by integrating frequency
    ph = 2*np.pi*np.cumsum(f0 + valArray*div)/sampRate
    t = np.r_[0.0:N]/sampRate
    iArray = np.cos(ph)
    jArray = np.imag(sig.hilbert(iArray))
    outArray = iArray + 1j*jArray

    # drop this down to baseband
    iDown = np.cos(2*np.pi*t*-fd)
    qDown = np.sin(2*np.pi*t*-fd)
    downArray = iDown + 1j*qDown
    outArray = outArray * downArray
    outArray = outArray.astype("complex64")
    return outArray

# CW morse code modulation
# note: quick script, not optomised, use for short messages only
def cw_mod(inputString, wdSpace, sampRate, div):
    cwArray = []
    wordCount = int(len(inputString))
    dotLenOn = int(sampRate*.10)
    dashLenOn = int(sampRate*.25)
    dotLenOff = int(sampRate*.10)
    dashLenOff = int(sampRate*.10)
    spaceLen = int(sampRate*wdSpace)
    for i in range(0,len(inputString)):
        for j in range(0,len(inputString[i])):
            if inputString[i][j] == ".":
                # dot
                cwArray.append([1]*dotLenOn)
                cwArray.append([0]*dotLenOff)
            else:
                #dash
                cwArray.append([1]*dashLenOn)
                cwArray.append([0]*dashLenOff)
            # add space between words
        cwArray.append([0]*spaceLen)
    cwArray_flat = []
    for sublist in cwArray:
        for item in sublist:
            cwArray_flat.append(item)

    array_size = len(cwArray_flat)
    # make an array to store sample times
    timeArray = np.zeros(array_size, dtype="float32")
    # make sinArray and cosArray
    sinArray = np.zeros(array_size, dtype="float32")
    cosArray = np.zeros(array_size, dtype="float32")
    # fill time array
    sampTime = 0.0
    for i in range(0,len(timeArray)):
        timeArray[i] = sampTime
        sampTime = sampTime + 1.0/sampRate
    # fill sin and cos arrays
    for i in range(0,len(cwArray_flat)):
        sinArray[i] = cwArray_flat[i] * np.sin(2*np.pi*div*timeArray[i])
        cosArray[i] = cwArray_flat[i] * np.cos(2*np.pi*div*timeArray[i])
    outArray = cosArray + 1j*sinArray
    outArray = outArray.astype("complex64")
    return outArray
