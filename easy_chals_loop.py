import sys
import time
import os

while True:
    print("starting morse code tx")
    print("===========================")
    os.system("python utils/file_tx.py -w morse_code_tx/morse-rfvillage_200ksps.fc32 -f 415500000 -r 200000 -g 40")
    print("done - configuring next tx")
    time.sleep(5)

    print("starting numbers tx")
    print("===========================")
    os.system("python utils/file_tx.py -w numbers_tx/numbers_800ksps.fc32 -f 146300000 -r 800000 -g 70")
    print("done - configuring next tx")
    time.sleep(5)
