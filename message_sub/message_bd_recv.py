import numpy as np
import sys
import socket
from termcolor import colored, cprint
sys.path.append('../utils/basic_modulation')
import basic_demod_utils as bdu
import matplotlib.pyplot as plt

UDP_IP = "127.0.0.1"
UDP_PORT = 6005
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

def find(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]

while True:
    data, addr = sock.recvfrom(33280) # buffer size
    sig = np.frombuffer(data, dtype="float32")
    x = bdu.bit_slice_floats(sig)
    y = bdu.clock_rec_basic(x, 250000, 9600)
    z = bdu.clock_rec_basic_cleanup(y)
    binary = z.tolist()
    index = 0
    for i in range(0, len(binary)):
        if binary[i] == binary[i+1]:
            index = i
            break
    binary = binary[index:]
    s = [str(i) for i in binary]
    res = "".join(s)
    n = 8
    res = [res[i:i+n] for i in range(0, len(res), n)]
    message = ""
    for i in range(len(res)):
        message = message+chr(int(res[i],2))
    f = find(message,"/")
    message = message[f[0]+1:f[1]]
    cprint("------------------------------", 'cyan','on_grey',attrs=['bold'])
    cprint(message.ljust(30), 'magenta','on_grey',attrs=['bold'])

    #plt.plot(y)
    #plt.show()
