#! /bin/sh


echo "STARTING BUILD SCRIPT"
echo "=============================\n"

(cd images_tx;
python3 tx_image_iq_gen.py;)

(cd morse_code_tx;
python3 morse_code_file_gen.py;)
