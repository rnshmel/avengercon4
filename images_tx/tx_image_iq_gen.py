# TX image gen

import sys
import os
import time
import numpy as np
sys.path.append('../utils/')
import basic_mod_utils as bmu

baud = 38400
samp_rate = 500000
sps = int(samp_rate/baud)

print("========================")
print("SPS: "+str(sps))
# BPSK
print("starting bpsk")
file_send = "cyber.jpg"
with open(file_send, "rb") as binary_file:
    # Read the whole file at once
    dat = bytearray(binary_file.read())
t = time.time()
dat = (b'\xaa\xaa\xaa\xaa\xff\x00')+dat+(b'\x31\x6d\xee\xbd')
x = bmu.bpsk_mod(dat, baud, samp_rate)
x.tofile("cyber-image_500ksps_bpsk.fc32")
elapsed = time.time() - t
print("TIME: "+str(elapsed))
print("done")
x = os.path.getsize(file_send)
# FSK
print("========================")
print("starting fsk")
file_send = "cyber.jpg"
with open(file_send, "rb") as binary_file:
    # Read the whole file at once
    dat = bytearray(binary_file.read())
t = time.time()
dat = (b'\xaa\xaa\xaa\xaa\xff\x00')+dat+(b'\x31\x6d\xee\xbd')
x = bmu.f_fsk_mod(dat, baud, samp_rate,50000)
x.tofile("cyber-image_500ksps_fsk.fc32")
elapsed = time.time() - t
print("done")
x = os.path.getsize(file_send)
print("TX TIME: "+ str((x*8.0)/baud))
