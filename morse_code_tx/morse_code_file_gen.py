# morse code file gen

import sys
import os
import time
import numpy as np
sys.path.append('../utils/')
import basic_mod_utils as bmu

print("starting")
t = time.time()
x = bmu.cw_mod([".-.","..-.","...-","..",".-..",".-..",".-","--.","."], .5, 200000, 1000)
x.tofile("morse-rfvillage_200ksps.fc32")
elapsed = time.time() - t
print("done")
print(elapsed)
