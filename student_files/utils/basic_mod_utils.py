# basic mod tools for generating IQ data to be played from an SDR
# mod tools are:
# BPSK mod - done
# QPSK mod - done
# DBPSK mod - in progress
# DQPSK mod - in progress
# FSK - done
# GFSK - in progress
# GMSK - in progress
# CW - in progress

import numpy as np
import scipy as sci
import sys
import struct
import time
import scipy.signal as sig

# faster FSK modulation
def f_fsk_mod(byteInput, baudRate, sampRate, div):
    Ns = int(sampRate/baudRate)
    f0 = div
    fd = 1.5*div
    Nbits = len(byteInput) * 8
    N = Nbits * Ns
    valArray = np.zeros(N, dtype="int8")
    # fill valArray
    index = 0 # keep track of current index
    for i in byteInput:
        for j in range(0,8):
            # for each byte, shift left and & with 128
            # this will pop off the left bits in order
            # save as "val"
            val = (i << j) & 128
            if val == 128: # a one
                for k in range(0,Ns):
                    valArray[index] = 1
                    index = index + 1
            else: # a zero
                for k in range(0,Ns):
                    valArray[index] = 0
                    index = index + 1
    # compute phase by integrating frequency
    ph = 2*np.pi*np.cumsum(f0 + valArray*div)/sampRate
    t = np.r_[0.0:N]/sampRate
    iArray = np.cos(ph)
    jArray = np.imag(sig.hilbert(iArray))
    outArray = iArray + 1j*jArray

    # drop this down to baseband
    iDown = np.cos(2*np.pi*t*-fd)
    qDown = np.sin(2*np.pi*t*-fd)
    downArray = iDown + 1j*qDown
    outArray = outArray * downArray
    outArray = outArray.astype("complex64")
    return outArray
