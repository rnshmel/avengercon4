import numpy as np
import scipy as sci
import sys
import struct
import time
import scipy.signal as sig
import os
import random as rand
import matplotlib.pyplot as plt

file_name = str(sys.argv[1]) # name of IQ file
x = np.fromfile(file_name, dtype="float32")
x = x.astype("float16")
plt.plot(x)
plt.show()
