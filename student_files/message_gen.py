import sys
import numpy as np
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QAction, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
sys.path.append('utils/')
import basic_mod_utils as bmu

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'FSK signal gen'
        self.left = 10
        self.top = 10
        self.width = 500
        self.height = 140
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # Create textbox
        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(400, 30)

        # Create a button in the window
        self.button = QPushButton('gen signal', self)
        self.button.move(20,80)

        # connect button to function on_click
        self.button.clicked.connect(self.on_click)
        self.show()

    @pyqtSlot()
    def on_click(self):
        textboxValue = self.textbox.text()
        textLen = len(textboxValue)
        if textLen > 30:
            QMessageBox.question(self, 'ALERT', "ERROR - too many characters", QMessageBox.Ok, QMessageBox.Ok)
            self.textbox.setText("")
        elif textLen == 0:
            QMessageBox.question(self, 'ALERT', "ERROR - NULL input", QMessageBox.Ok, QMessageBox.Ok)
            self.textbox.setText("")
        else:
            QMessageBox.question(self, 'ALERT', "IQ files generated", QMessageBox.Ok, QMessageBox.Ok)
            msg = bytearray("UUUUUU/"+textboxValue+"/.","utf8")
            #print(msg)
            c64 = bmu.f_fsk_mod(msg, 9600, 2000000, 200000)
            x = np.zeros(2000000,dtype="complex64")
            c64 = np.concatenate((x,c64))
            c64 = np.concatenate((c64,x))
            #c64.tofile("text_2msps.fc32")
            i = c64.real * 127
            q = c64.imag * 127
            i = i.astype("int8")
            q = q.astype("int8")
            cs8 = np.empty((i.size + q.size,), dtype=i.dtype)
            cs8[0::2] = i
            cs8[1::2] = q
            cs8.tofile("text_2msps.sc8")
            sys.exit()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
